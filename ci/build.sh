#!/usr/bin/env sh

set -e
set -v

CI_VARIABLES=ci-variables.tfvars
OVERRIDES_FILE=ci-overrides.tf

echo '
aws_access_key_id = "string"
aws_secret_access_key = "string"
aws_region = "string"
certificate_arn = "string"
api_domain_name = "string"
api_custom_domain_stage_name = "string"
dns_zone_id = "string"
' > ${CI_VARIABLES}

echo 'provider "aws" {
  region     = "string"
  access_key = "string"
  secret_key = "string"
}
' >  ${OVERRIDES_FILE}

terraform init
terraform validate -var-file ${CI_VARIABLES} .

rm -f ci-*.tf*
