locals {
  api_fqdn = "${var.api_host_name}.${var.api_domain_name}"
}

resource "aws_api_gateway_rest_api" "api" {
  name        = "${var.api_name}"
  description = "${var.api_description}"
  policy      = "${var.api_policy}"

  api_key_source = "${var.api_key_source}"

  endpoint_configuration {
    types = "${var.endpoint_types}"
  }
}

resource "aws_api_gateway_authorizer" "cognito" {
  name          = "${var.api_name}-cognito"
  rest_api_id   = "${aws_api_gateway_rest_api.api.id}"
  type          = "${var.api_authorizer_type}"
  provider_arns = ["${var.api_authorizer_provider_arns}"]
}

resource "aws_api_gateway_domain_name" "api" {
  certificate_arn = "${var.certificate_arn}"
  domain_name     = "${local.api_fqdn}"
}

resource "aws_route53_record" "api" {
  name    = "${aws_api_gateway_domain_name.api.domain_name}"
  type    = "A"
  zone_id = "${var.dns_zone_id}"

  alias {
    evaluate_target_health = true
    name                   = "${aws_api_gateway_domain_name.api.cloudfront_domain_name}"
    zone_id                = "${aws_api_gateway_domain_name.api.cloudfront_zone_id}"
  }
}

resource "aws_route53_record" "api_ipv6" {
  name    = "${aws_api_gateway_domain_name.api.domain_name}"
  type    = "AAAA"
  zone_id = "${var.dns_zone_id}"

  alias {
    evaluate_target_health = true
    name                   = "${aws_api_gateway_domain_name.api.cloudfront_domain_name}"
    zone_id                = "${aws_api_gateway_domain_name.api.cloudfront_zone_id}"
  }
}

resource "aws_api_gateway_base_path_mapping" "test" {
  api_id      = "${aws_api_gateway_rest_api.api.id}"
  stage_name  = "${var.api_custom_domain_stage_name}"
  domain_name = "${aws_api_gateway_domain_name.api.domain_name}"
}
