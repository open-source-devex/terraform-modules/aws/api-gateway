variable "api_name" {
  default = "my-api"
}

variable "api_description" {
  default = "API Created by module ososdx/api-gateway"
}

variable "api_policy" {
  default = ""
}

variable "endpoint_types" {
  type    = "list"
  default = ["EDGE"]
}

variable "api_key_source" {
  default = "HEADER"
}

variable "api_authorizer_type" {
  default = "COGNITO_USER_POOLS"
}

variable "api_authorizer_provider_arns" {
  type        = "list"
  default     = []
  description = "Required when api_authorizer_type = COGNITO_USER_POOLS and should be the ARN of the Cognito User Pool"
}

variable "certificate_arn" {}

variable "api_host_name" {
  default = "api"
}

variable "api_domain_name" {}

variable "dns_zone_id" {}

variable "api_custom_domain_stage_name" {}
